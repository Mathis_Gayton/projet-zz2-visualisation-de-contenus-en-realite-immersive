﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YoutubeExplode;
using System.Threading;
using YoutubeExplode.Models.MediaStreams;

namespace ExtractionVideos
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(String[] args)
        {

            var url = args[0];
            var id = YoutubeClient.ParseVideoId(url);

            var client = new YoutubeClient();

            var streamInfoSet = await client.GetVideoMediaStreamInfosAsync(id);


            var streamInfo = streamInfoSet.Muxed.WithHighestVideoQuality();

            var ext = streamInfo.Container.GetFileExtension();

            await client.DownloadMediaStreamAsync(streamInfo, @args[1] + @"\" + args[2] + "." + ext);

            Console.WriteLine(@args[1] + @"\" + args[2] + "." + ext);
        }
    }
}
