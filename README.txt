Informations d'utilisation :
- Le lecteur de documents n'est pas encore intégré en réalité virtuelle : l'utiliser sur un PC.
- La première exécution des programmes externes peut être bloquée par les antivirus. Il faut rajouter les .exe aux exclusions de l'antivirus.
- Au préalable, remplir la ligne 15 du fichier Assets/Scripts/PdfReader.cs en renseignant dans le string tempDirectory le chemin d'un dossier où stocker temporairement les images d'output de la conversion PDF.
ATTENTION : Ce dossier doit être VIDE, et son chemin ne doit pas contenir d'espace.
	
Utilisation du lecteur de documents :

1) Lire un PDF 
:Lire un PDF qui est sur le PC en sélectionnant son dossier, puis le fichier.
Consignes :
	Utilisable pour des PDF de moins de 1000 pages. Pour augmenter cette limite, changer FileBrowser/Program.cs :
		ligne 23 : Changer le %4 en %i, avec i la puissance de 10 du nombre de pages limite ;
		ligne 27 : Dans GhostscriptWrapper.GeneratePageThumbs(), le 4ème paramètre correspond au nombre de pages limite.
Commandes disponibles :
	Axe horizontal (e.g. touches du clavier) : Passer à la page précédente ou suivante.
	"Cancel" (e.g. touche Échap) : Revenir au menu principal.

2) Lire une vidéo
:Lire une vidéo, soit sur le PC, soit à partir de YouTube. Dans ce dernier cas, il faut renseigner l'URL de la vidéo, puis le dossier et le nom sous lesquels l'importer.
Consignes :
	Ne pas mettre d'espace ou de caractéres spéciaux comme des accents dans le nom et le chemin sous lesquels la vidéo YouTube sera enregistrée.
Commandes disponibles :
	"Play" (e.g. touche Espace) : Mettre en pause/lecture la vidéo.
	Clic gauche sur la vidéo : Active le mode déplacement.
		Clic droit sur la vidéo en mode déplacement : Désactive le mode déplacement, et ancre la vidéo à l'endroit indiqué.
	"Cancel" (e.g. touche Échap) : Revenir au menu principal.

Liste des scripts :
Les scripts se trouvent dans le dossier Assets/Scripts.
Général :
	Deplacement.cs : Intéractions basiques avec la scène
	MainMenu.cs : Menu principal permettant de choisir le type de fichier que l'on veut lire
	MenuLink.cs : Retour aumenu principal en faisant "Cancel"
PDF :
	PdfReader.cs : Lecture et navigation dans un PDF
Vidéo :	
	LancerDisque.cs : Lance une vidéo à partir du disque du PC
	LancerYouTube.cs : Lance une vidéo à partir de YouTube
	PlayVideo.cs : Pause/Lecture de la vidéo
	SongProgressBar.cs : Barre de son
	VideoProgressBar.cs : Barre de progression dans la vidéo
	PauseVideo.cs : mettre en pause et reprendre la vidéo