﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileBrowser
{
    public partial class Form2 : Form
    {
        public string imageFolder;
        public Form2()
        {
            InitializeComponent();
        }

        private void folderOpen_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if(fdb.ShowDialog() == DialogResult.OK)
            {
                imageFolder = fdb.SelectedPath;
                Console.WriteLine(imageFolder);
                this.Close();
            }
        }
    }
}
