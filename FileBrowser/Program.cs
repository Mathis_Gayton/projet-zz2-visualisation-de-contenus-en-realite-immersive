﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GhostscriptSharp;

namespace FileBrowser
{
    static class Program
    {
        [STAThread]
        static void Main(string[] outputDirectory)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 myForm1 = new Form1();
            Application.Run(myForm1);
            string inputPath = myForm1.pdfPath;
            string fileName = Path.GetFileNameWithoutExtension(inputPath);
            string outputPath = outputDirectory[0] + @"\" + fileName + @"%4d.png"; //4 cractères pour le nommage individuel == 1000 pages
            Console.WriteLine(inputPath);
            Console.WriteLine(outputPath);
 
            GhostscriptWrapper.GeneratePageThumbs(inputPath,outputPath,1,999,400,400); // Limite de 1000 pages
        }
    } 
}
