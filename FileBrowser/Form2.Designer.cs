﻿namespace FileBrowser
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.folderOpen = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.folderPath = new System.Windows.Forms.Label();
            this.folderBrowser = new System.Windows.Forms.ListView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // folderOpen
            // 
            this.folderOpen.Location = new System.Drawing.Point(903, 562);
            this.folderOpen.Name = "folderOpen";
            this.folderOpen.Size = new System.Drawing.Size(75, 23);
            this.folderOpen.TabIndex = 0;
            this.folderOpen.Text = "OpenFolder";
            this.folderOpen.UseVisualStyleBackColor = true;
            this.folderOpen.Click += new System.EventHandler(this.folderOpen_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // folderPath
            // 
            this.folderPath.AutoSize = true;
            this.folderPath.Location = new System.Drawing.Point(31, 566);
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(55, 13);
            this.folderPath.TabIndex = 1;
            this.folderPath.Text = "folderPath";
            // 
            // folderBrowser
            // 
            this.folderBrowser.Location = new System.Drawing.Point(12, 12);
            this.folderBrowser.Name = "folderBrowser";
            this.folderBrowser.Size = new System.Drawing.Size(966, 542);
            this.folderBrowser.TabIndex = 2;
            this.folderBrowser.UseCompatibleStateImageBehavior = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(92, 563);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(805, 20);
            this.textBox1.TabIndex = 3;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 595);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.folderBrowser);
            this.Controls.Add(this.folderPath);
            this.Controls.Add(this.folderOpen);
            this.Name = "Form2";
            this.Text = "Folder Browser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button folderOpen;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label folderPath;
        private System.Windows.Forms.ListView folderBrowser;
        private System.Windows.Forms.TextBox textBox1;
    }
}