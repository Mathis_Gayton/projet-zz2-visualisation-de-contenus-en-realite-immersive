﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChoixYoutube
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 myForm1 = new Form1();
            Application.Run(myForm1);
            /*Console.WriteLine(myForm1.url);
            Console.WriteLine(myForm1.nomFichier);
            Console.WriteLine(myForm1.dossier);*/
            Console.WriteLine(myForm1.url + " " + myForm1.dossier + " " + myForm1.nomFichier);
        }
    }
}
