﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChoixYoutube
{
    public partial class Form1 : Form
    {

        public string url;
        public string nomFichier;
        public string dossier;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            url = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            nomFichier = textBox2.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if (fdb.ShowDialog() == DialogResult.OK)
            {
                dossier = fdb.SelectedPath;
                Console.WriteLine(dossier);
                this.Close();
            }
        }
    }
}
