# Projet ISIMA 2e année pour Atos
## Intégration de documents (PDF, PowerPoint, Vidéos) sous Unity 

Réalisé en binôme durant l'année 2019-2020, l'objectif du projet est de pouvoir visualiser différents documents sous Unity.

## Fonctionnalités

Le projet permet de visualiser des PDF et des vidéos sous Unity. Pour visualiser un PowerPoint, il faut d'abord le sauvegarder sous un format PDF.

Pour le PDF, le programme permet de : 
- Choisir le PDF que l'on souhaite importe à l'aide d'un Windows form
- Transformer le PDF en images (une page = une image)
- Importer les images dans Unity
- A l'aide des touches du clavier, faire défiler les images

Un thread permet d'effectuer des actions pendant que les images sont importées dans Unity.

Pour les vidéos : 
- Choisir une vidéo sur son disque OU choisir une vidéo sur YouTube
- Importer la vidéo sur Unity (ou la télécharger puis l'importer sur Unity)
- Mettre la vidéo en pause / relancer la vidéo
- Avancer / reculer dans la vidéo à l'aide d'une barre d'avancement
- Régler le son à l'aide d'une barre
- "Prendre" la vidéo et la déplacer dans le monde de Unity. 

## Réalisation

La partie PDF a été réalisée grâce à un NuGet de Visual Studio, GhostScriptSharp. Il permet de transformer un PDF en images. [Github ici](https://github.com/mephraim/ghostscriptsharp)
La sélection des fichiers que l'on souhaite ouvrir a été réalisée grâce aux Windows Form de Visual Studio.

Le projet est fonctionnel avec la version 2019.3 de Unity. /!\ Peut ne plus être fonctionnel sur les versions d'après.
