﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Redirection sur le lecteur PDF ou vidéos selon le bouton sur lequel on appuie.
 * Les lecteurs sont d'abord désactivés pour afficher le menu, puis l'on active celui que l'on a sélectionné. */
public class MainMenu : MonoBehaviour
{
    GameObject pdf, video;

    //Awake est la première fonction à être appelée dans l'ordre d'exécution général
    public void Awake()
    {
        // Find ne fonctionne que pour les objects actifs : on garde en mémoire PDF et Vidéo
        pdf = GameObject.Find("PDFPréfab");
        video = GameObject.Find("VidéoPréfab");

        // On désactive les préfabs dès le début
        pdf.SetActive(false);
        video.SetActive(false);

        // Activation des lecteurs si click
        gameObject.transform.Find("LirePDF").gameObject.GetComponentInChildren<Button>().onClick.AddListener(activatePDF);
        gameObject.transform.Find("LireVidéo").gameObject.GetComponent<Button>().onClick.AddListener(activateVideo);
    }

    void activatePDF()
    {
        GameObject instance = Instantiate(pdf);
        instance.name = "PDFInstance";
        instance.SetActive(true);
        gameObject.SetActive(false);
        //gameObject.GetComponent<Renderer>().enabled = false; // le menu disparaît
    }

    void activateVideo()
    {
        GameObject instance = Instantiate(video);
        instance.name = "VidéoInstance";
        instance.SetActive(true);
        gameObject.SetActive(false);
    }
}
