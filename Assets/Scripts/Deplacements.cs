﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deplacements : MonoBehaviour
{
    private float _deltaX;
    private float _deltaZ;
    public float _speed;

    private float _mouseX;
    private float _mouseY;
    public float _speedMouse;

    public GameObject Ecran;
    // Start is called before the first frame update
    void Start()
    {
        _speed = 15.0f;
        _speedMouse = 1.0f;
        _mouseX = 0.0f;
        _mouseY = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        _deltaX = Time.deltaTime * Input.GetAxis("Horizontal");
        _deltaZ = Time.deltaTime * Input.GetAxis("Vertical");
        gameObject.GetComponent<Transform>().Translate(_deltaX * _speed, 0, _deltaZ * _speed);

        if(!Input.GetKey(KeyCode.LeftShift))
        {
            _mouseX += Input.GetAxis("Mouse X") * _speedMouse;
            _mouseY += Input.GetAxis("Mouse Y") * _speedMouse;
            _mouseY = Mathf.Clamp(_mouseY, -90.0f, 90.0f);
        }

        gameObject.GetComponent<Transform>().eulerAngles = new Vector3(-_mouseY, _mouseX, 0);

        if(Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                Debug.Log("Touché : " + hit.collider.gameObject.name);
                if(hit.collider.gameObject.name == "Ecran")
                {
                    hit.collider.gameObject.GetComponent<Transform>().SetParent(gameObject.GetComponent<Transform>());
                    hit.collider.gameObject.GetComponent<Transform>().eulerAngles = new Vector3(0, 180, 0);
                }
            }
        }

        if(Input.GetButtonDown("Fire2"))
        {
            if(gameObject.transform.childCount != 0)
            {
                foreach(Transform child in gameObject.transform)
                {
                    child.SetParent(null);
                }
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            if (Ecran.GetComponent<Transform>().parent == gameObject.GetComponent<Transform>())
            {
                Ecran.GetComponent<Transform>().Translate(new Vector3(0, 0, -Input.GetAxis("Mouse ScrollWheel") * 5));
            }
        }
    }
}
