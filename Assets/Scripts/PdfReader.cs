﻿using System.Collections.Generic; // Utilisation de la List<>
using System.Collections.Concurrent; // Utilisation de collections thread-safe (queue)
using System.Diagnostics;
using System.IO; // Récupération des informations sur les fichiers 
using System.Threading; //Thread pour la transformation PDF -> Images
using UnityEngine.UI;
using UnityEngine;

public class PdfReader : MonoBehaviour
{
    private string converterPath;
    private string outputDirectory;
    private int page; //Navigation entre les images 
    private PdfRenderer pdf;
    readonly string tempDirectory = @"C:\Users\Mathis\Desktop\Projet Atos\Projet Fini\Projet Atos ZZ2\RepertoirAChanger"; /*A COMPLETER*/

    /* Variables du thread */
    private Thread threadPdfImages;
    Process procPdfImages;
    Queue<string> filePathQueue; // File FIFO des chemins des images
    ConcurrentQueue<byte[]> imageConcurrentQueue; // File FIFO de data d'images


    void Start()
    {
        converterPath = Application.dataPath.Remove(Application.dataPath.Length - "Assets".Length).Replace(@"/", @"\") + @"FileBrowser\bin\Release\FileBrowser.exe";
        outputDirectory = tempDirectory;

        pdf = new PdfRenderer(gameObject); 

        filePathQueue = new Queue<string>();
        imageConcurrentQueue = new ConcurrentQueue<byte[]>(); 
        threadPdfImages = new Thread(pdfToImages);
        
        threadPdfImages.Start();
        page = 1;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (procPdfImages != null) 
                procPdfImages.Kill();
        }

        /* Chargement des images dans la scène */
        if (threadPdfImages.IsAlive || !imageConcurrentQueue.IsEmpty) 
        {
            Texture2D texture;
            GameObject page;

            if (imageConcurrentQueue.TryDequeue(out byte[] imageBytes))
            {
                texture = LoadRessources.LoadTextureFromBytes(imageBytes);
                page = pdf.NewPage(texture);
                UnityEngine.Debug.Log("Nombre d'images dans la file d'images : " + imageConcurrentQueue.Count +
                                        " Nombre d'images importées dans la scène : " + pdf.Length());
            }
        }

        /*Défilement des images en désactivant les Renderer*/
        /*Ne marche pas si les images ont des morceaux transparents */
        UnityEngine.Debug.Log("LA PAGE VAUT :" + page);
        UnityEngine.Debug.Log("Le total VAUT :" + pdf.Length());
        GameObject pageActuelle = gameObject.transform.Find("Page " + page).gameObject; 
        GameObject pageSuivante = gameObject.transform.Find("Page " + (page + 1)).gameObject;

        if (Input.GetKeyDown(KeyCode.T))
        {
            pageSuivante.GetComponent<Image>().enabled = true;
            //UnityEngine.Debug.Log(pageActuelle);
            if (page < pdf.Length() - 1)
            {
                page++;
            }
            else
            {
                UnityEngine.Debug.Log("Vous ne pouvez pas avancer");
            }
        }
        else if (Input.GetKeyDown(KeyCode.Y))
        {
            if (page == 1)
            {
                UnityEngine.Debug.Log("Début du document");
            }
            else
            {
                if(pageSuivante.GetComponent<Image>().IsActive() == true) /*cas de la derniere page*/
                {
                    pageSuivante.GetComponent<Image>().enabled = false;
                }
                else
                {
                    pageActuelle.GetComponent<Image>().enabled = false;
                    page--;
                }
            }
        }
        UnityEngine.Debug.Log("Vous êtes à la " + page + "ème page"); 
    }

    /* Thread pour charger les images dans le programme */
    void pdfToImages()
    {
        Directory.CreateDirectory(outputDirectory);

        string[] filePathArray;

        /* Lancement de la conversion */
        procPdfImages = new Process();
        ProcessStartInfo startInfo = new ProcessStartInfo(converterPath, "\"" + outputDirectory + "\"");
        procPdfImages = Process.Start(startInfo);
        UnityEngine.Debug.Log("THREAD : Conversion PDF commencée");

        // Tant que la conversion externe n'est pas terminée ou qu'il reste des pages à charger
        do
        {            
            // Rescensement des images dans le dossier
            filePathArray = Directory.GetFiles(outputDirectory);
            filePathQueue = new Queue<string>(filePathArray);
            UnityEngine.Debug.Log("THREAD : Nombre d'images dans le dossier " + outputDirectory + " : " + filePathArray.Length);

            // Chargement des images du dossier dans le programme
            while (filePathQueue.Count > 0)
            {
                try
                {
                    UnityEngine.Debug.Log("THREAD : Essai pour charger " + filePathQueue.Peek());
                    LoadRessources.LoadBytesFromPath(out byte[] imageBytes, filePathQueue.Peek());
                    if (imageBytes != null) 
                        imageConcurrentQueue.Enqueue(imageBytes);
                    UnityEngine.Debug.Log("THREAD : Essai pour charger " + filePathQueue.Peek() + " réussi");
                }
                catch (System.Exception e)
                {
                    UnityEngine.Debug.Log(e.Message);
                    break; //on arrête d'essayer de charger les autres images
                }
                UnityEngine.Debug.Log("THREAD : Supprime le fichier");
                File.Delete(filePathQueue.Dequeue()); // suppression des images traitées pour ne plus les avoir à la prochaine itération
            }
            UnityEngine.Debug.Log("THREAD : Nombre d'images chargées dans la file : " + imageConcurrentQueue.Count);
        }
        while (!(procPdfImages.HasExited) || (filePathQueue.Count > 0));

        UnityEngine.Debug.Log("THREAD : Toutes les images ont été importées dans le programme.");
    }
}

class PdfRenderer
{
    /* Déclaration des attributs */
    private GameObject _parentPanel;
    private int _pageCount;
    private int _currentPage;
    private List<GameObject> _pageList;

    /* Définition des méthodes */
    public PdfRenderer(GameObject parentPanel)
    {
        _parentPanel = parentPanel;
        _pageCount = 0;
        _currentPage = 1; //Pas de page
        _pageList = new List<GameObject>();
    }
    public int GetCurrentPage()
    {
        if (_pageCount > 0)
        {
            return _currentPage;
        }
        else
            return 0;

    }
    public bool GoToNextPage()
    {
        if (_currentPage < _pageCount)
        {
            _pageList[_currentPage - 1 + 1].SetActive(true);
            ++_currentPage;
            UnityEngine.Debug.Log("Page actuelle : " + _currentPage);

            return true;
        }
        else
            return false;
    }
    public bool GoToPreviousPage()
    {
        if (_currentPage > 1)
        {
            _pageList[_currentPage - 1].SetActive(false);
            ++_currentPage;
            UnityEngine.Debug.Log("Page actuelle : " + _currentPage);

            return true;
        }
        else
            return false;
    }
    public int Length()
    {
        return _pageCount;
    }
    public GameObject NewPage(Texture2D texture)
    {
        GameObject page = new GameObject();
        ++_pageCount;
        page.name = "Page " + _pageCount;                      // Nommage pour retrouver facilement l'objet
        Image pageImage = page.AddComponent<Image>();       //Ajout du component Image au gameObject                                                 
        page.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f); //Dimmensionnage des images en gardant le ratio
        pageImage.sprite = LoadRessources.LoadSpriteFromTexture2D(texture);
        page.GetComponent<RectTransform>().SetParent(_parentPanel.transform);        //Le nouvel objet devient fils du canvas
        page.SetActive(true);   
        page.transform.position = _parentPanel.transform.position;        //Centrage de l'image par rapport au cavas
        page.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        if(_pageCount > 1)
        {
            page.GetComponent<Image>().enabled = false; /*Pour n'afficher que la 1e page*/
        }
        _pageList.Add(page);

        return page;
    }
}

public static class LoadRessources
{
    /* Chargement d'un fichier en tableau de bytes */ 
    public static void LoadBytesFromPath(out byte[] fileBytes, string filePath) 
    {
        fileBytes = null;
        try
        {
            fileBytes = File.ReadAllBytes(filePath);
        }
        catch (System.Exception e)
        {
            fileBytes = null;
            UnityEngine.Debug.LogError(e.Message);
            throw;
        }
    }

    /*Création d'une texture 2D à partir de bytes*/
    public static Texture2D LoadTextureFromBytes(byte[] fileBytes)
    {
        Texture2D texture;
        texture = new Texture2D(2, 2);           // Create new "empty" texture
        texture.LoadImage(fileBytes);
        //texture.LoadRawTextureData(fileBytes); // Load the imagedata into the texture (size is set automatically)
                                               // texture.Apply();  //pour refresh le CPU
        UnityEngine.Debug.Log("Texture chargée");

        return texture;                 // If data = readable -> return texture
    }

    /*Création d'un sprite à partir d'une texture 2D*/
    public static Sprite LoadSpriteFromTexture2D(Texture2D texture, float pixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight)
    {
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height) , new Vector2(0, 0), pixelsPerUnit, 0, spriteType);
        return sprite;
    }
}

