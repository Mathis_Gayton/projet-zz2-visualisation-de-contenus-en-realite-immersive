﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayVideo : MonoBehaviour
{
    VideoPlayer video;
    void Start()
    {
        video = gameObject.GetComponent<VideoPlayer>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Play"))
        {
            if (video.isPlaying)
            {
                video.Pause();
            }
            else
            {
                video.Play();
            }
        }
    }
}
