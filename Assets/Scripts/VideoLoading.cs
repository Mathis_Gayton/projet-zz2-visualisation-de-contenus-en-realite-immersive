﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;
using System.Diagnostics;
using System.Text.RegularExpressions;

public class VideoLoading : MonoBehaviour
{
    private VideoPlayer video;
    public GameObject Ecran;

    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer mesh = Ecran.GetComponent<MeshRenderer>();
        Material materielVideo = mesh.material;

        video = gameObject.GetComponent<VideoPlayer>();
        video.source = VideoSource.Url;

        /*Process myApp = new Process();
        myApp = Process.Start(@"C:\Users\Mathis\Desktop\Projet Atos\Vidéo\ExtractionVideos\ExtractionVideos\bin\Release\netcoreapp3.1\ExtractionVideos.exe");
        myApp.WaitForExit();*/

         var p = new Process();
         p.StartInfo.UseShellExecute = false;
         p.StartInfo.RedirectStandardOutput = true;
         p.StartInfo.FileName = @"C:\Users\Mathis\Desktop\Projet Atos\Vidéo\FileBrowser vidéos\FileBrowser vidéos\bin\Debug\FileBrowser vidéos.exe";
         p.Start();

         string output = p.StandardOutput.ReadToEnd();
         p.WaitForExit();

         UnityEngine.Debug.Log(output);
         string replacement = Regex.Replace(output, @"\t|\n|\r", "");
         UnityEngine.Debug.Log(replacement);

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (video.isPlaying)
            {
                video.Pause();
            }
            else
            {
                video.Play();
            }
        }
    }
    public void CheckDimensions(string url, Material m)
    {
        GameObject tempVideo = new GameObject("Temp video for " + url);
        VideoPlayer videoPlayer = tempVideo.AddComponent<VideoPlayer>();
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
        videoPlayer.targetTexture = new RenderTexture(1, 1, 0);
        UnityEngine.Debug.Log("Le dynamic scale " + videoPlayer.targetTexture.useDynamicScale);
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = url;
        videoPlayer.prepareCompleted += (VideoPlayer source) =>
        {
            UnityEngine.Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); // do with these dimensions as you wish
            loadVideo(source.texture.width, source.texture.height, m);
            Destroy(tempVideo);
        };
        videoPlayer.Prepare();
    }

    public void loadVideo(float largeur, float hauteur, Material m)
    {
        Ecran.GetComponent<Transform>().localScale = new Vector3(largeur / 100.0f * 1.2f, hauteur / 100.0f * 1.2f, 0.01f);
        RenderTexture myTex = new RenderTexture((int)largeur, (int)hauteur, 0);
        video.renderMode = VideoRenderMode.RenderTexture;
        video.targetTexture = myTex;
        m.mainTexture = myTex;
        m.SetTexture("_EmissionMap", myTex);
    }
}
