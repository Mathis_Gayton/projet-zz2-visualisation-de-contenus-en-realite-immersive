﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Diagnostics;
using System.Text.RegularExpressions;

public class LancerYoutube : MonoBehaviour
{

    public VideoPlayer video;
    public GameObject ecran;
    public Canvas canv;

    string url;
    string chemin;
    string nom;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponentInChildren<Text>().text = "Video sur YouTube";
        video.source = VideoSource.Url;
        gameObject.GetComponentInChildren<Button>().onClick.AddListener(test); // Appel de la fonction avec le clic
    }

    public void test()
    {

        /*Processus lancant le windows form permettant de récupérer les parametres de la video : url, dossier de sauvegarde, nom de fichier*/

        string[] mots;
        Process choixVideo = new Process();
        choixVideo.StartInfo.UseShellExecute = false;
        choixVideo.StartInfo.RedirectStandardOutput = true;
        choixVideo.StartInfo.FileName = Application.dataPath.Remove(Application.dataPath.Length-"Assets".Length).Replace(@"/", @"\") + @"ChoixYoutube\ChoixYoutube\bin\Debug\ChoixYoutube.exe"; // Windows form permettant de choisir la video Youtube // Chemin indépendamment du PC
        choixVideo.Start();

        string output = choixVideo.StandardOutput.ReadToEnd(); // Récupération de la sortie standard
        UnityEngine.Debug.Log(output);

        choixVideo.WaitForExit();
        mots = output.Split(' ');
        url = mots[0];
        chemin = mots[1];
        nom = mots[2];

        string[] parseurURL = url.Split('\n'); //La récupération de l'url est mauvaise, on recupere aussi le dossier dedans, on Split le string pour ne garder que l'url
        url = parseurURL[1];
        UnityEngine.Debug.Log(url);

        /*On enlève les espaces et autres caracteres*/

        chemin = Regex.Replace(chemin, @"\t|\n|\r", "");
        UnityEngine.Debug.Log(chemin);

        nom = Regex.Replace(nom, @"\t|\n|\r", "");
        UnityEngine.Debug.Log(nom);

        /*Processus permettant de télécharger la video à l'aide des parametres recupérés via windows form precedemment*/

        Process p = new Process();
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.FileName = Application.dataPath.Remove(Application.dataPath.Length - "Assets".Length).Replace(@"/", @"\") + @"\ExtractionVideos\ExtractionVideos\bin\Debug\netcoreapp3.1\ExtractionVideos.exe"; // Appel de l'extracteur youtube
        p.StartInfo.Arguments = url + " " + chemin + " " + nom; // Arguments de l'exécutable
        p.Start();

        string output2 = p.StandardOutput.ReadToEnd(); // Récupération de la sortie standard
        p.WaitForExit();

        string replacement = Regex.Replace(output2, @"\t|\n|\r", "");
        UnityEngine.Debug.Log(replacement);

        MeshRenderer mesh = ecran.GetComponent<MeshRenderer>();
        Material materielVideo = mesh.material;

        video.url = (replacement);
        CheckDimensions(video.url, materielVideo);
        canv.gameObject.SetActive(false);
        video.Play();
    }

    /*Creation d'une render texture inutile pour récupérer les veritables dimensions de la video*/
    public void CheckDimensions(string url, Material m)
    {
        GameObject tempVideo = new GameObject("Temp video for " + url);
        VideoPlayer videoPlayer = tempVideo.AddComponent<VideoPlayer>();
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
        videoPlayer.targetTexture = new RenderTexture(1, 1, 0);
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = url;
        videoPlayer.prepareCompleted += (VideoPlayer source) =>
        {
            UnityEngine.Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height);
            loadVideo(source.texture.width, source.texture.height, m);
            Destroy(tempVideo);
        };
        videoPlayer.Prepare();
    }

    /*Instanciation de la bonne texture avec les bons parametres*/
    public void loadVideo(float largeur, float hauteur, Material m)
    {
        ecran.GetComponent<Transform>().localScale = new Vector3(largeur / 100.0f * 1.2f, hauteur / 100.0f * 1.2f, 0.01f);
        RenderTexture myTex = new RenderTexture((int)largeur, (int)hauteur, 0);
        video.renderMode = VideoRenderMode.RenderTexture;
        video.targetTexture = myTex;
        m.mainTexture = myTex;
        m.SetTexture("_EmissionMap", myTex);
    }
}