﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLink : MonoBehaviour
{
    private GameObject mainMenu;

    //Awake est la première fonction à être appelée dans l'ordre d'exécution général
    void Awake()
    {
        // Garder une référence au menu principal pour pouvoir y retourner
        mainMenu = GameObject.Find("Menu principal");
    }

    // Veut-on sortir du lecteur et retourner au menu principal ?
    void Update()
    {
        if (Input.GetButtonUp("Cancel"))
        {
            mainMenu.SetActive(true);
            Object.Destroy(gameObject);
        }
    } 
}
