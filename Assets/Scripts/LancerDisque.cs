﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Video;
using System.Diagnostics;
using System.Text.RegularExpressions;

public class LancerDisque : MonoBehaviour
{
    public VideoPlayer video;
    public GameObject ecran;
    public Canvas canv;

    private bool estLancee = false;

    void Start()
    {
        video.source = VideoSource.Url;

        gameObject.GetComponentInChildren<Text>().text = "Video sur le disque";
        gameObject.GetComponentInChildren<Button>().onClick.AddListener(Disque); // Appel de la fonction lors du clic
    }

    void Disque()
    {
        MeshRenderer mesh = ecran.GetComponent<MeshRenderer>();
        Material materielVideo = mesh.material;


        /*Processus permettant de chosir la video sur le disque via windows form et renvoie sur la sortie standard l'emplacement de la video*/
        var p = new Process();
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.FileName = Application.dataPath.Remove(Application.dataPath.Length-"Assets".Length).Replace(@"/", @"\") + @"FileBrowser vidéos\FileBrowser vidéos\bin\Debug\FileBrowser vidéos.exe"; // Chemin indépendamment du PC
        p.Start();
        string output = p.StandardOutput.ReadToEnd(); /*Lecture sortie strandard*/
        p.WaitForExit();

        string replacement = Regex.Replace(output, @"\t|\n|\r", ""); /*On enleve les caracteres speciaux espaces / sauts de lignes etc*/
        UnityEngine.Debug.Log(replacement);

        video.url = (replacement);
        CheckDimensions(video.url, materielVideo);
        canv.gameObject.SetActive(false);
        video.Play();
        estLancee = true;
    }

    /*Creation d'une render texture inutile pour récupérer les veritables dimensions de la video*/
    public void CheckDimensions(string url, Material m)
    {
        GameObject tempVideo = new GameObject("Temp video for " + url);
        VideoPlayer videoPlayer = tempVideo.AddComponent<VideoPlayer>();
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
        videoPlayer.targetTexture = new RenderTexture(1, 1, 0);
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = url;
        videoPlayer.prepareCompleted += (VideoPlayer source) =>
        {
            UnityEngine.Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); 
            loadVideo(source.texture.width, source.texture.height, m);
            Destroy(tempVideo);
        };
        videoPlayer.Prepare();
    }
    
    /*Instanciation de la bonne texture avec les bons parametres*/
    public void loadVideo(float largeur, float hauteur, Material m)
    {
        ecran.GetComponent<Transform>().localScale = new Vector3(largeur / 100.0f * 1.2f, hauteur / 100.0f * 1.2f, 0.01f);
        RenderTexture myTex = new RenderTexture((int)largeur, (int)hauteur, 0);
        video.renderMode = VideoRenderMode.RenderTexture;
        video.targetTexture = myTex;
        m.mainTexture = myTex;
        m.SetTexture("_EmissionMap", myTex);
    }

}
